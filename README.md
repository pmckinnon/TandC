```
GET / ? url: Uri
  -> render(template(:interstitial, url))
  
# e.g. url="https://goo.gl/jeCNdu"
```

```
HEAD / ? url: Uri
  -> render(head(url))
```

----------------------------------------------------------------------
### interstitial.template(url: Uri)
##### vβ
```xml
<html>
    <body>
        <a class="terms-and-conditions" href="https://medium.com/@mckinnon/google-counsel-dac712e8ffd">Terms and Conditions.</a>
          <br/>
        <a class="button continue" href="{{{url}}}">I Agree, Continue;</a>
    </body>
</html>
```